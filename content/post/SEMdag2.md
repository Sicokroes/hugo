---
title: Dag 2 Design challenge 2
date: 2017-11-20
---
*Mijn taak*
Ik heb de taak op mij genomen om teamleider te zijn dit kwartaal en heb meegedaan aan de standup meeting met alle teamcaptains. Hier heb ik voorgesteld om 09:30 te beginnen in plaats van 09:20 omdat dat makkelijker is voor iedereen.
*Taken als team*
We hebben als team gewerkt aan de debriefing en het merkanalyse we hebben hierop feedback gevraagd en zijn de rest van de dag bezig geweest met het verbeteren en hebben gewerkt aan onze teamafspraken en samenwerkingscontract. Hierbij zijn we ons beter bewust geworden van onze sterke en zwakke punten als team. 
