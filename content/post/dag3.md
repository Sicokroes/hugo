---
title: donderdag Studiodag 3
subtitle: Onderzoek Hogeschool rotterdam
date: 2017-09-07
---

*Persoonlijke taken*
Vandaag heb ik me bezig gehouden met de vragen van de enquête. Deze vragen gelden voor de eerstejaars studenten aan CMD. Ik heb actief meegedaan aan het bedenken van de vragen en ze uitgetypt. 

*Taken als team* 
Als team zijn we naar Design theorie geweest. Daar kregen we de opdracht om met de techniek die we geleerd hadden een scene uit te schetsen van een opdracht uit het verleden. Hierbij ging het vooral om het proces van situatie naar resultaat, en dat hierbij gebruik gemaakt wordt van: schetsen, concept, prototype. We hebben toen de beste uitgekozen als team en die aan de klas gepresenteerd. 

