---
title: Studiodag 8
subtitle: Overleggen
date: 2017-09-27
---

*Persoonlijke taken*
Ik heb voor vandaag het spel blackstories mee genomen zodat we een goede opstart zouden hebben, en dat we hiermee onze hersens aan het werk kunnen zetten en onze creativiteit activeren. Ik heb actief mee gedacht aan het spel.

*Taken als team*
We hebben als team ons bezig gehouden met verder brainstormen over de game en een aantal ideeen genoteerd. Dit gaan we later verder bespreken met de team leden die er die dag niet waren. 