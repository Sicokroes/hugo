---
title: Studiodag 11
subtitle: Presenteren
date: 2017-10-23
---

*Persoonlijke taken*
Vandaag begonnen we zoals gewoonlijk met een potje black stories om ons brijn een beetje op gang te kregen voor de rest van de dag, later op de dag heb ik mee gedacht aan wat voor extra element we misschien zouden toe kunnen voegen aan de wachttijd die elk team in onze game zou hebben. Ik heb hierbij duidelijk mijn mening gegeven, en we zijn later tot een overeenstemming gekomen. Later heb ik voorgesteld om een filmpje te maken voor onze expositie. Ik heb voorgesteld om gelijk naar buiten te gaan om zoveel mogelijk beeld materiaal te verzamelen.

*Taken als team*
We hebben als team een promofilmpje gemaakt gemaakt voor onze app/game en hebben verder na gedacht over de expositie.  