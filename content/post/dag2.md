---
title: woensdag Studiodag 2
subtitle: visualiseren
date: 2017-09-06
---

*Persoonlijke taken*
Ik heb me vandaag vooral bezig gehouden met het bedenken van de game en het visualiseren daarvan. Het leek mij een tof idee om de game in hetzelfde thema te doen als in “find Waldo”. Mijn idee was om de peercoaches zich te laten verspreiden in de stad met bepaalde outfits. Jij krijgt als speler aan het begin een kledingstuk en hiermee moet je de eerste peercoach zoeken in een drukke winkelstraat. Dit idee heb ik verder uitgewerkt door te schetsen. 

*Taken als team* 
Als team hebben we het idee voor de game uitgewerkt. We hebben de planning aangepast en we hebben ons concept aangepast en een nieuwe doelgroep gekozen die hier beter bij past. 
