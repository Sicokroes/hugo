---
title: Dag 8 Design challenge 2
date: 2017-12-20
---
*Mijn taak*
Vandaag heb ik meegedaan aan de captain meeting. Daarbij is gebleken dat we erg goed opweg zijn als team en bijna alles gevalideerd is. Ik heb meegeholpen aan het paper prototype om vandaag te testen. Bij het testen heb ik de vragen gesteld aan de testpersonen.
*Taken als team*
We hebben als team ons prototype getest.  
