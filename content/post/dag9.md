---
title: Studiodag 9
subtitle: Overleggen
date: 2017-10-02
---

*Persoonlijke taken*
Ik heb actief mee gedaan met het uitwerken van het uiteindelijke concept van het spel. We hebben het oude idee aangepast omdat we hier feedback op hadden gekregen en we zijn er achter gekomen dat het handigger zou zijn om bijde teams in onze game op pad te sturen inplaats van de teams op te splitsen en verdeeldheid te veroorzaken. Ik heb schetsen gemaakt voor de app, ik heb namen bedacht over hoe de game zou kunnen heten.

*Taken als team*
Als team hebben we een besluit genomen en zijn daarmee aan de slag gegaan. We hebben het zo aangepast dat we meer een doel hadden in het spel.