---
title: Dag 4 Design challenge 2
date: 2017-11-27
---
*Mijn taak* 
Ik begon de dag gezamenlijk met alle teamcaptains met een stand-up meeting. Daarna heb ik mijn Pitch voorbereid en gezorgd dat we alle benodigdheden hadden om te laten zien. Vervolgens heb ik de Pitch gehouden aan fabrique. De pitch ging redelijk goed, we moeten alleen het concept aanpassen zodat het beter past bij wat de doelgroep wilt.
*Als team*
We hebben als team verder gebrainstormd over een nieuw concept, maar we zijn nog nergens op uit gekomen. 
