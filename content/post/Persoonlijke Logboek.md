---
title: Studiodag 1
subtitle: Onderzoek Hogeschool rotterdam
date: 2017-09-04
---

*Persoonlijke taken*
Vandaag heb ik mezelf aan het werk en heb ik onderzoek gedaan naar alle studies van de Hogeschool Rotterdam. Ik heb toen gekeken wat de meest relevante studies waren en die het meest aansloten op CMD. Ik heb een mappenstructuur aangemaakt voor onze groeps drive die we de komende tijd kunnen gebruiken en waar we al ons werk in kwijt kunnen. Ik heb actief meegedacht over het onderzoek en de game. Ook heb ik me bezig gehouden met de inventarisatie van het team. Ik heb aan iedereen gevraagd wat onze kwaliteiten zijn als team, wat de achtergrond is van elk teamlid, wat onze ambitie is, wat onze visie is en wat de regels zijn die voor ons team gelden. Daarnaast heb ik nog een aantal vragen bedacht die we kunnen gebruiken in een interview voor onze doelgroep. 


*Taken als team*
Als eerst hebben we als team gebrainstormd, we hebben onderzoek gedaan op het gebied van games, we hebben onderzocht welke studies de Hogeschool Rotterdam bied. We hebben een doelgroep gekozen voor onze game, en voor de doelgroep hebben we een Enquête gemaakt.

