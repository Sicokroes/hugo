---
title: Dag 10 Design challenge 2
date: 2017-01-10
---
*Mijn taak*
Ik ben vandaag bij de captain meeting geweest, daarnaast heb ik met jordan gewerkt aan de user-journey. Ik heb ook een layar account aangemaakt zodat die gekoppeld kon worden aan het r&b filmpje. Daarnaast ben ik naar het validatie moment geweest van de design rationale. 
*Taken als team*
We hebben als team de kaarten afgemaakt. We hebben de user journey en visuele styleguide laten valideren. Daarnaast hebben we Hennad geïnterviewd en een testplan gemaakt. 
