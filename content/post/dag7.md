---
title: Studiodag 7
subtitle: Brainstormen
date: 2017-09-25
---

*Persoonlijke taken*
Ik begon de dag met het verbeteren van mijn moodboard. Ook heb ik de spel analyse afgemaakt. Toen heb ik iedereen opgetrommeld om de enquête uit te printen en die in de klas te verspreiden zodat we verder konden met het braintormen over onze game.

*Taken als team*
Als team hebben we een brainstorm sessie gehouden. We hebben hierbij de techniek gebrukt die we bij theorie geleerd hebben. We hebben als eerst al onze ideeen op sticknotes gezet en later hebben we die verdeeld onder 4 catagorien. Een kant was de realistische kant en de ander de onrealistische. Dit was vercolgens weer gesplists met een kant met orginele ideeen en een kant met normale ideeen. Toen dat gedaan was zijn we gaan filteren en zijn toen op het idee gekomen om onze game met audio te gaan doen. 