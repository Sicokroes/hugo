---
title: Studiodag 6
subtitle: Onderzoek Hogeschool rotterdam
date: 2017-09-18
---

*Persoonlijke taken*
We begonnen de dag met presentaties, we hebben de taken verdeeld over wie wat zou zeggen. Ik heb vooral veel verteld over het onderzoek en de taakverdeling. Ook heb ik wat verteld over hoe we op het idee kwam van ons spel. 

*Taken als team*
we hebben als team geluisterd naar alle presentaties, deze hebben we besproken en vergeleken met ons eigen presentatie en paper prototype. we zijn tot de conclusie gekomen dat ons spel goed werkte omdat het erg specifiek was. Dit gaan we proberen te gebruiken voor ons aankomend nieuwe concept. 
