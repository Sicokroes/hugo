---
title: Dag 1 Design challenge 2
date: 2017-11-15
---
*Mijn taak*
Ik begon de dag met het reflecteren met mijn groepje van de vorige periode. Ik heb gedacht aan alles wat goed gegaan is en heb benoemd dat we zeker een goede werksfeer hadden binnen ons team en dat dat te blijken viel aan onze motivatie. Daarna heb ik me aangesloten bij Jordan als een start voor ons nieuwe groepje. 
*Taken als team*
Als team gingen we aan de slag met een teamnaam. We hebben gelijk een gezamenlijke drive aangemaakt waarop we al onze bestanden kunnen delen. Ook zijn we begonnen met een trello pagina waarop we al onze activiteiten bijhouden en onze taken kunnen verdelen. Vervolgens zijn we dus met die taken aan de slag gegaan. Lotte en ik hebben ons bezig gehouden met de “debrefing” en Jiska en Jordan hebben zich bezig gehouden met de merkanalyse. 
