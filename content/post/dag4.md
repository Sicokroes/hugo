---
title: Woensdag Studiodag 4
subtitle: Onderzoek Hogeschool rotterdam
date: 2017-09-11
---

*Persoonlijke taken*
Ik begon de dag met een schets voorbeeld van de game. Ik vond het erg belangrijk om hierbij te denken aan gebruiksvriendelijkheid en goed gebruik te maken van de ruimte op het scherm. Ik dacht dat ik alle belangrijke punten had benoemd, maar toen ik naar de schetsen van Aidan keen kwam ik erachter dat ik toch heel wat punten over het hoofd gezien had. Later heb ik me bezig gehouden met de cursus voor het beheren van een blog. Daarna heb ik meegeholpen met het maken van de paper prototype. 

*Taken als team*
We begonnen de dag met allemaal een schets te maken voor de paper prototype en daarna een frame wat lijkt op een telefoon waarin we de pagina’s kunnen weergeven van de game. Hiermee gingen we verder na de blog cursus. 
