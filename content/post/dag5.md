---
title: Woensdag Studiodag 5
subtitle: Onderzoek Hogeschool rotterdam
date: 2017-09-11
---

Persoonlijke taken
Ik heb een beginnetje gemaakt met het spelanalyse, hierbij heb ik een stukje geschreven over hoe ons spel in elkaar zit. Toen was het tijd om ons spel te presenteren aan de klas en het docententeam. Ik heb toen nog een paar belangrijke punten verteld, zoals dat het mogelijk is om in de game verschillende virtuele kledingstukken te kopen maar dat, dat niet essentieel is en dat je het ook met minder kledingstukken kan voltooien. Tijdens het testen van de game heb ik me voorgedaan als peercoach en heb ik mij verstopt in het gebouw van de hoogeschool. Ik heb een aanwijzing gegeven van waar ik ongeveer zat maar niet precies de locatie, zodat de kandidaten mij konden zoeken. 

Taken als team
Als team hebben we onze paper prototype getest. We zijn als team allemaal aanwezig geweest bij alle lessen van de dag, inclusief de lessen van onze studiecoach, waar we ons  weer hebben bezig gehouden met onze sterke en zwakke punten als team. 
 
