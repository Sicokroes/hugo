---
title: Studiodag 11
subtitle: Presenteren
date: 2017-10-09
---

*Persoonlijke taken*
Vandaag heb ik met Remco naar de kaarten en de raadsels gekeken want bij de feedback kregen we te horen dat de vragen op onze kaartjes wat moeiliker mogen. Ik heb me hierbij profecioneel opgesteld door serieus mee te denken en goed samen te werken. 

*Taken als team*
Vandaag begonnen we met inventarisatie 3, we hebben onze feedback besproken en gingen verder met brainstormen. 